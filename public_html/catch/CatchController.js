app.controller('CatchController', ['$scope', '$location', 'CatchService', function ($scope, $location, cs) {
        $scope.isEdit = cs.isEdit;
        $scope.currentCatch = {
            species: '',
            weight: '',
            length: '',
            catchTime: new Date(),
            longitude: '',
            latitude: '',
            location: '',
            lure: '',
            technique: '',
            depth: '',
            weather: '',
            airTemperature: '',
            waterTemperature: '',
            base64Image:''
        };

        $scope.getPreview = function (input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();
                reader.onload = function (e) {
                    $scope.currentCatch.base64Image =  e.target.result;
                    document.getElementById('image').src = $scope.currentCatch.base64Image;
                };
                reader.readAsDataURL(input.files[0]);
            }
        };

        $scope.getMyCatches = function () {
            cs.getMyCatches()
                    .success(function (data, status, headers, config) {
                        $scope.catches = data;
                    });
        };

        $scope.getSpecies = function () {
            cs.getSpecies()
                    .success(function (data, status, headers, config) {
                        $scope.species = data;
                    });
        };
        
        $scope.add = function () {
            cs.addCatch($scope.currentCatch)
                    .success(function (data, status, headers, config) {
                        console.log("Catch added");
                    })
                    .error(function (data, status, headers, config) {
                        if (data.fieldErrors) {
                            $scope.form.$setPristine();
                            $scope.fieldErrors = data.fieldErrors;
                        }
                    });
        };

        $scope.save = function () {
            console.log("Saved");
            $scope.isEdit = false;
        };
        $scope.cancel = function () {
            console.log("Cancelled");
            $scope.isEdit = false;
        };

        $scope.startEdit = function (id) {
            for (var i = 0; i <= $scope.catches.length; i++) {
                if ($scope.catches[i].id === id) {
                    cs.catch = $scope.catches[i];
                    break;
                }
            }
            $location.path('/catch/edit');
        };

        $scope.getSpecies();
        $scope.getMyCatches();
        if (cs.catch !== null) {
            $scope.currentCatch = cs.catch;
        }
    }]);
