//angular.module('Services', [])
app.factory('CatchService', ['$http', 'restSettings', function ($http, rs) {
            var catchService = {};

            catchService.catch = null;
            catchService.getSpecies = function () {
                return $http.get(rs.url + 'species');
            };
            
            catchService.addCatch = function (c) {
                return $http.put(rs.url + 'catch', c);
            };
            
            catchService.getMyCatches = function () {
                return $http.get(rs.url + 'catch/my');
            };
            
            return catchService;
        }]);

