app.controller('HomeController', ['$scope', '$location', 'userService', function ($scope, $location, us) {
        $scope.$watch(
                function () {
                    return us.isLoggedIn();
                },
                function (newValue, oldValue) {
                    console.log('old: ' + oldValue + ', new: ' + newValue);
                    $scope.loggedIn = newValue;
                });
        $scope.loggedIn = false;
        $scope.logout = function () {
            us.logout();
            $location.path('/').replace();
        };
        $scope.error = null;
    }]);
