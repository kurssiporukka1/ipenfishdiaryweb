app.factory('userService', ['$http', 'restSettings', function ($http, rs) {
        var userService = {};

        userService.isLoggedIn = function () {
            return localStorage.getItem('token') !== null;
        };

        userService.register = function (user) {
            return $http.post(rs.url + 'user/register', user);
        };

        userService.login = function (user) {
            return $http.post(rs.url + 'user/login', user);
        };

        userService.logout = function () {
            localStorage.removeItem('token');
            this.loggedIn = false;
        };
        
        return userService;
}]);
