/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

var app = angular.module("FishDiaryApp", ['ngRoute', 'angularify.semantic.modal', 'angularify.semantic.dropdown', 'dateParser']);

app.config(['$httpProvider', function ($httpProvider) {
        $httpProvider.interceptors.push('requestInterceptor');
    }]);

app.config(['$routeProvider', function ($routeProvider) {
        $routeProvider
                .when('/', {
                    templateUrl: 'home/home.html'
                })
                .when('/register', {
                    templateUrl: 'register/register.html',
                    controller: 'RegisterController'
                })
                .when('/login', {
                    templateUrl: 'login/login.html',
                    controller: 'LoginController'
                })
                .when('/browse', {
                    templateUrl: 'browse/list.html',
                    controller: 'BrowseController'
                })
                .when('/catch', {
                    templateUrl: 'catch/list.html',
                    controller: 'CatchController'
                })
                .when('/catch/add', {
                    templateUrl: 'catch/add.html',
                    controller: 'CatchController'
                })
                .when('/catch/edit', {
                    templateUrl: 'catch/edit.html',
                    controller: 'CatchController'
                })
                .otherwise({
                    redirectTo: '/'});
    }]);



 