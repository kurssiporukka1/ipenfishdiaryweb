app.controller('BrowseController', ['$scope', '$routeParams', 'browseService', 'userService', 'restSettings', function ($scope, $routeParams, bs, us, rs) {
        $scope.catches;
        $scope.imageSrc = '';
        $scope.catchDetail;
        $scope.show_details = false;
        $scope.show_image = false;
        $scope.commentText = '';
        $scope.search = {
            user: '',
            species: '',
            weight: ''
        };

        $scope.loggedIn = function () {
            return us.isLoggedIn();
        };

        $scope.showDetails = function (id) {
            for (var i = 0; i <= $scope.catches.length; i++) {
                if ($scope.catches[i].id === id) {
                    $scope.catchDetail = $scope.catches[i];
                    if ($scope.catchDetail.imageName) {
                        $scope.imageSrc = 'http://192.168.0.102:8080/Fishdiary/image?id=' + id;;
//                        $scope.imageSrc = 'http://localhost:8080/Fishdiary/image?id=' + id;
                    } else {
                        $scope.imageSrc = 'images/noimage.jpg';
                    }
                    $scope.getComments(id);
                    break;
                }
            }
            $scope.show_details = true;
        };

        $scope.closeImage = function () {
            $scope.show_image = false;
        };

        $scope.getCatches = function () {
            bs.getAllCatches($scope.search)
                    .success(function (data, status, headers, config) {
                        $scope.catches = data;
                    });
        };
        
        $scope.getComments = function (id) {
            bs.getComments(id)
                    .success(function (data, status, headers, config) {
                        $scope.comments = data;
            });
        };
        
        $scope.addComment = function () {
            bs.addComment($scope.catchDetail.id, $scope.commentText);
        };

        $scope.getCatches();
    }]);
