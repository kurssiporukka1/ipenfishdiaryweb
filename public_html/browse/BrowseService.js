app.factory('browseService', ['$http', 'restSettings', function ($http, rs) {
        browseService = {};
        browseService.getAllCatches = function (search) {
            return $http.get(rs.url + 'catch', {params: search});
        };
        
        browseService.getComments = function (catchId) {
            return $http.get(rs.url + 'comment/' + catchId);
        };
        
        browseService.addComment = function (catchId, text) {
            return $http.put(rs.url + 'comment/' + catchId, {text: text});
        };
        
        return browseService;
    }]);