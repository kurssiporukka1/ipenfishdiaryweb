app.factory('requestInterceptor', function () {
    var requestInterceptor = {
        request: function (config) {
            config.headers['AuthToken'] = localStorage.getItem('token');
            return config;
        }
    };
    
    return requestInterceptor;
});