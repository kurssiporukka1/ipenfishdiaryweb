app.controller('LoginController', ['$scope', '$location', 'userService', function ($scope, $location, us) {
        $scope.user = {
            email: '',
            password: ''
        };
        $scope.loading = false;
        $scope.error = null;
        $scope.login = function () {
            $scope.loading = true;
            us.login($scope.user)
                    .success(function (data, status, headers, config) {
                        localStorage.setItem('token', data.token);
                        console.info('Login ok');
                        $scope.loading = false;
                        $location.path('/').replace();
                    })
                    .error(function (data, status, headers, config) {
                        console.error('Login failed');
                        $scope.error = data.error;
                        $scope.loading = false;
                    });
        };
    }]);

