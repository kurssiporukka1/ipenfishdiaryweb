app.controller('RegisterController', ['$scope', 'userService', function ($scope, us) {
        $scope.error = null;
        $scope.showMsg = false;
        $scope.noMatch = false;
        $scope.loader = false;
        $scope.user = {
            name: '',
            email: '',
            password: '',
            password2: ''
        };
        $scope.msg = {
            header: '',
            content: ''
        };
        $scope.register = function () {
            $scope.noMatch = false;
            $scope.error = null;
            $scope.showMsg = false;
            $scope.loader = true;

            if ($scope.user.password !== $scope.user.password2) {
                $scope.noMatch = true;
                return;
            } else {
                $scope.noMatch = false;
            }
            
            us.register($scope.user)
                    .success(function (data, status, headers, config) {
//                        $scope.msg.header = "Rekisteröityminen onnistui";
//                        $scope.msg.content = "Tervetuloa käyttämään palvelua. JHJdasd dfdsfd dsff sdfdsf fsg ";
                        $scope.showMsg = true;
                        localStorage.setItem('token', data.token);
                        $scope.loader = false;
                        $scope.registerSuccess = true;
                    })
                    .error(function (data, status, headers, config) {
                        if (data.error) {
                            $scope.error = data.error;
                        }
                        if (data.fieldErrors) {
                            $scope.fieldErrors = data.fieldErrors;
                            $scope.form.$setPristine();
                        }
                        $scope.loader = false;
                    });
        };
    }]);
